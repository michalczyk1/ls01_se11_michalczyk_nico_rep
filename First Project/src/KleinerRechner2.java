import java.util.Scanner;
//Scanner Klasse muss importiert werden, um Nutzereingaben einlesen zu k�nnen

public class KleinerRechner2 {
	
	public static void main(String[] args) {
		
		//wir erstellen einen eigenen Scanner, Namens "sc"
		//der Name kann beliebig gew�hlt werden
		Scanner sc = new Scanner (System.in);
		int zahl1, zahl2;
		int ergebnis = 0;
		String plus = "+";
		String minus = "-";
		String mal = "*";
		String geteilt = "/";
		String operation;
		int i = 0;
		
		System.out.println("Geben Sie die erste Zahl ein");
		zahl1 = sc.nextInt();  //mit Hilfe des Scanners sc wird ein Integer-Wert eingelesen
		
		System.out.println("Geben Sie die zweite Zahl ein");
		zahl2 = sc.nextInt();
		
		
	while(i == 0)	{
		
		System.out.println("Bitte geben Sie die Rechnoperation an (+;-;*;/)");
		operation = sc.next();
		
		if(operation.equals(plus)) {
			ergebnis = zahl1 + zahl2; 
			i++;
		}
		
		else if(operation.equals(minus)) {
			ergebnis = zahl1 - zahl2; 
			i++;
		}
		else if(operation.equals(mal)) {
			ergebnis = zahl1 * zahl2; 
			i++;
		}
		else if(operation.equals(geteilt)) {
			ergebnis = zahl1 / zahl2; 
			i++;
		}
		else {
			System.out.println("Ung�ltige Rechenoperation, bitte geben ein g�ltiges Zeichen ein.");
		}
	}
		// ergebnis = zahl1 + zahl2; 
		
		System.out.println("Das Ergebnis ist: " + ergebnis);
		
		/* h�bschere Ausgabe: 
		System.out.println("Das Ergebnis ist: ");
		System.out.println(zahl 1 + " + " + zahl2 + " = " + ergebnis);
		 */
		// 5eMMrwPXyPLgfKv4emGN
		sc.close();  //der Scanner muss geschlossen werden

	}
}
