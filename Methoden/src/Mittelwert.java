
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double a = 2.0;
      double b = 4.0;
      double c = 100.0;
      double d = 75.0;
      double mittelwert;
    
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
     
      mittelwert = berechneMittelwert(c,d);
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", c, d, mittelwert );
   }
   static double berechneMittelwert(double x, double y) 
   { 
      return (x + y) / 2.0; 
   } 
}
