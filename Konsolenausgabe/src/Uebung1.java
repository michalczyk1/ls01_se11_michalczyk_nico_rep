
public class Uebung1 {

	public static void main(String[] args) {
		
	// Aufgabe 1	
		
		String zwei = "Und das ist der zweite Satz.";
		
		System.out.print("Dies ist die erste Aufgabe.\n");
		System.out.printf( "%20s\n", zwei );
		
		// print() gibt die Eingabe ohne Zeilenbruch aus und println() gibt die Eingabe aus und f�gt einen Zeilenbruch ein
	
	
	
	// Aufgabe 2
		
		String muster = "***********";
		System.out.printf("\n%6.1s\n %4.1s %1.1s\n", muster, muster, muster);
		System.out.printf("%4.1s %3.1s\n", muster, muster);
		System.out.printf("%3.1s %5.1s\n", muster, muster);
		System.out.printf("%2.1s %7.1s\n", muster, muster);
		System.out.printf("%1.1s %9.1s\n", muster, muster);
		System.out.printf("%s\n", muster);
	
		
	// Aufgabe 3
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551; 
		double e = 97.34;
		
		System.out.printf("\n%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
		
	}

}
