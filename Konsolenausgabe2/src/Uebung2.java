
public class Uebung2 {

	public static void main(String[] args) {
	
	// Aufgabe 1	
		
		String muster = "**";
		System.out.printf("\n%6.2s\n%2.1s%7.1s\n", muster, muster, muster);
		System.out.printf("%2.1s%7.1s\n", muster, muster);
		System.out.printf("%6.2s\n", muster);
		
	// Aufgabe 2
		
		int a = 1;
		int b = 2;
		int c = 6;
		int d = 24;
		int e = 120;
		String eins = "1!";
		String zwei = "2!";
		String drei = "3!";
		String vier = "4!";
		String f�nf = "5!";
		String ende = "= 1 * 2 * 3 * 4 * 5";
		
		System.out.printf( "%-5s%-19.3s =%4d\n", eins, ende, a );
		System.out.printf( "%-5s%-19.7s =%4d\n", zwei, ende, b );
		System.out.printf( "%-5s%-19.11s =%4d\n", drei, ende, c );
		System.out.printf( "%-5s%-19.15s =%4d\n", vier, ende, d );
		System.out.printf( "%-5s%-19s =%4d\n\n\n", f�nf, ende, e );
		
		
	// Aufgabe 3
		
		int f1 = -20; 
		int f2 = -10;
		String f3 = "+0";
		String f4 =  "+20";
		String f5 = "+30";
		
		double c1 = -28.8889; 
		double c2 = -23.3333;
		double c3 = -17.7778;
		double c4 = -6.6667;
		double c5 = -1.1111;
		
		String F = "Farenheit";
		String C = "Celsius";
		
		System.out.printf( "%-12s|%10s\n", F, C);
		System.out.println( "------------------------\n");
		System.out.printf( "%-12d|%10.2f\n", f1, c1);
		System.out.printf( "%-12d|%10.2f\n", f2, c2);
		System.out.printf( "%-12s|%10.2f\n", f3, c3);
		System.out.printf( "%-12s|%10.2f\n", f4, c4);
		System.out.printf( "%-12s|%10.2f\n", f5, c5);


	}

}
