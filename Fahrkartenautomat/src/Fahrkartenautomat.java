import java.util.Scanner;


class Fahrkartenautomat
{
	public static final Scanner tastatur = new Scanner(System.in);
    public static void main(String[] args) {
    	 
       float zuZahlenderBetrag; 
       float r�ckgabebetrag2;
       float r�ckgabebetrag1;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
      
       r�ckgabebetrag1 = fahrkartenBezahlen(zuZahlenderBetrag);
       r�ckgabebetrag2 = (float) (Math.round(r�ckgabebetrag1*100.0)/100.0);
       
       fahrkartenAusgeben();
       
       rueckgeldAusgeben(r�ckgabebetrag2);
       
      
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       System.out.print("Wiederholen? (J/N) : ");
       String eingabe = tastatur.next();
      if(eingabe.equals("J")){
         main(null);
      }
         tastatur.close();
       
       
    	
    	 }
    
    



	
    public static float fahrkartenbestellungErfassen() {
		
		
    	String[] ticket =  {"1 Einzelfahrschein Berlin AB: ", "2 Einzelfahrschein Berlin BC: ", "3 Einzelfahrschein Berlin ABC: ",
    						"4 Kurzstrecke: ", "5 Tageskarte Berlin AB: ", "6 Tageskarte Berlin BC: ", "7 Tageskarte Berlin ABC: ",
    						"8 Kleingruppen-Tageskarte Berlin AB: ", "9 Kleingruppen-Tageskarte Berlin BC: ", 
    						"10 Kleingruppen-Tageskarte Berlin ABC: " 
    	};																																// Arrays f�r Fahrkarten Namen und Preise erstellt
    	float[] preise = {2.90f, 3.30f, 3.60f, 1.90f, 8.60f, 9.00f, 9.60f, 23.50f, 24.30f, 24.90f  };
		
		for(int i = 0; i<=9; i++) {
			System.out.printf("%-40s" + "%20.2f" + "� \n",ticket[i], preise[i]); 
		}
		System.out.println("0 Fertig");
		
		
		System.out.println("Bitte w�hlen Sie die Tickets aus, die Sie kaufen m�chten, wenn Sie fertig sind "
							+ "w�hlen Sie bitte Fertig aus.");
		float gesammtBetrag = 0.00f;
		while(true) {							// Die Schleife wird nun so lange ausgef�hrt bis der Benutzer keine Tickets mehr haben m�chte und die 0 w�hlt, flasche Eingaben werden ignoriert und der Benutzer wird darauf hingewiesen
			
			byte auswahl;
			System.out.print("Ticketnummer: ");
			auswahl = tastatur.nextByte();
			if(auswahl == 0) {																	
				break;
			}
			if(auswahl < 0 || auswahl > ticket.length) {
				System.out.println("Ung�ltige Eingabe, bitte geben Sie g�ltige Zahl ein!");
			}
			else {
				gesammtBetrag = gesammtBetrag +  preise[auswahl-1];
				System.out.printf("Gesammtpreis: %.2f \n", gesammtBetrag);
			}
			
			
		}
		System.out.printf("Gesammtpreis: %.2f \n", gesammtBetrag);
		
    	
		return (gesammtBetrag);
	}
    
    
	
	public static float fahrkartenBezahlen(float zuZahlen) {
		
		float eingezahlterGesamtbetrag = 0.00f;
		float eingeworfeneM�nze;
		
	       while(eingezahlterGesamtbetrag < zuZahlen)	// Geld wird vom Benutzer eingezahlt
	       {
	    	   System.out.printf("Noch zu zahlen: " + "%.2f" + " Euro \n", zuZahlen - eingezahlterGesamtbetrag); 
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextFloat();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
	  
		return(eingezahlterGesamtbetrag - zuZahlen);
	}

	public static void fahrkartenAusgeben() {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	       return;
		
	}
	public static void rueckgeldAusgeben(float r�ckgeld) {
		
		if(r�ckgeld > 0.00)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f" + " EURO", r�ckgeld);
	    	   System.out.println(" wird in folgenden M�nzen ausgezahlt: \n");
	    	   int x = 0;
	    	   byte[] r�ckgabeWert = new byte[25];
	    	   String[] r�ckgabeTyp = new String [25];
	    	   
	           while(r�ckgeld >= 2.00) // 2 EURO-M�nzen
	           {
	        	  r�ckgabeWert[x] = 2;						// Die benutzten M�nzen bei der R�ckgeldausgebe werden in zwei Arrays gespeichert (Wert und Typ der M�nze), um Sie dann grafisch darzustellen
	        	  r�ckgabeTyp[x] = "EURO";
	        	  x++;
	        	  
	        	  r�ckgeld -= 2.00;
	        	  
	           }
	           while(r�ckgeld >= 1.00) // 1 EURO-M�nzen
	           {
		        	  r�ckgabeWert[x] = 1;
		        	  r�ckgabeTyp[x] = "EURO";
		        	  x++;
	        	  r�ckgeld -= 1.00;
	           }
	           while(r�ckgeld >= 0.50) // 50 CENT-M�nzen
	           {
		        	  r�ckgabeWert[x] = 50;
		        	  r�ckgabeTyp[x] = "CENT";
		        	  x++;
	        	  r�ckgeld -= 0.50;
	           }
	           while(r�ckgeld >= 0.2) // 20 CENT-M�nzen
	           {
		        	  r�ckgabeWert[x] = 20;
		        	  r�ckgabeTyp[x] = "CENT";
		        	  x++;
	        	  r�ckgeld -= 0.20;
	 	         
	           }
	           while(r�ckgeld >= 0.09) // 10 CENT-M�nzen
	           {
		        	  r�ckgabeWert[x] = 10;
		        	  r�ckgabeTyp[x] = "CENT";
		        	  x++;
	        	  r�ckgeld -= 0.1;
		         
	           }
	   
	           while(r�ckgeld >= 0.04)// 5 CENT-M�nzen
	           {
		        	  r�ckgabeWert[x] = 5;
		        	  r�ckgabeTyp[x] = "CENT";
		        	  x++;
	        	  r�ckgeld -= 0.05;
	           }
	          
	       
	           int y = x%3;
	           if(x == 2) {
	        	   y = 1;
	           }
	           else if(x == 3 || x == 6) {
	        	   y = x/3;
	           }
	           else if(x == 4) {
	        	   y = 2;
	           }
	        
	        String vorlage1 = "* * *";
	    	String vorlage2 = "*       *";
	    	String vorlage3 = "*";
	       	int z = 0;
	       	int w = 0;
	           for(int i = 0; i < y ; i++) {
	        	   
	   			System.out.printf("%9s", vorlage1);					// Die M�nzen werden in Dreierreihen ausgegeben
	   			if(r�ckgabeWert[1] != 0 || r�ckgabeWert[4] != 0) {  // Hier wird kontrollliert ob die 2. & 5. M�nze ungleich Null sind, damit diese ausgegeben werden k�nnen 
	   				System.out.printf("%12s", vorlage1);
	   			} 
	   			if(r�ckgabeWert[2] != 0 || r�ckgabeWert[5] != 0) { // Hier dasselbe mit der 3. & 6. M�nze
	   				System.out.printf("%12s", vorlage1);
	   			}
	   			System.out.println();
	   			
	   			System.out.printf("%11s", vorlage2);
	   			if(r�ckgabeWert[1] != 0 || r�ckgabeWert[4] != 0) {
	   				System.out.printf("%12s", vorlage2);
	   			} 
	   			if(r�ckgabeWert[2] != 0 || r�ckgabeWert[5] != 0) {
	   				System.out.printf("%12s", vorlage2);
	   			}
	   			System.out.println();
	   			
	   			System.out.printf("%2s%5d%5s", vorlage3, r�ckgabeWert[z], vorlage3);
	   			z++;
	   			if(r�ckgabeWert[1] != 0 || r�ckgabeWert[4] != 0) {
	   				System.out.printf("%2s%5d%5s", vorlage3, r�ckgabeWert[z],vorlage3);
		   			z++;
	   			} 
	   			if(r�ckgabeWert[2] != 0 || r�ckgabeWert[5] != 0) {
	   				System.out.printf("%2s%5d%5s", vorlage3, r�ckgabeWert[z],vorlage3);
		   			z++;
	   			} 
	   			System.out.println();
	   			
	   			System.out.printf("%2s%7s%3s", vorlage3,r�ckgabeTyp[w], vorlage3);
	   			w++;
	   			if(r�ckgabeWert[1] != 0 || r�ckgabeWert[4] != 0) {
	   				System.out.printf("%2s%7s%3s", vorlage3, r�ckgabeTyp[w],vorlage3);
		   			w++;
	   			} 
	   			if(r�ckgabeWert[2] != 0 || r�ckgabeWert[5] != 0) {
	   				System.out.printf("%2s%7s%3s", vorlage3, r�ckgabeTyp[w],vorlage3);
		   			w++;
	   			}
	   			System.out.println();
	   			
	   			System.out.printf("%11s", vorlage2);
	   			if(r�ckgabeWert[1] != 0 || r�ckgabeWert[4] != 0) {
	   				System.out.printf("%12s", vorlage2);
	   			} 
	   			if(r�ckgabeWert[2] != 0 || r�ckgabeWert[5] != 0) {
	   				System.out.printf("%12s", vorlage2);
	   			} 
	   			System.out.println();
	   			
	   			System.out.printf("%9s", vorlage1);
	   			if(r�ckgabeWert[1] != 0 || r�ckgabeWert[4] != 0) {
	   				System.out.printf("%12s", vorlage1);
	   			} 
	   			if(r�ckgabeWert[2] != 0 || r�ckgabeWert[5] != 0) {
	   				System.out.printf("%12s", vorlage1);
	   			}
	   			System.out.println();
	   			r�ckgabeWert[2] = 0; // hier werden die M�nzen 2 und 3 aus dem Array gel�scht, damit, falls es einen zweiten durchlauf gibt, die M�nzen nicht f�lschlicherweise ausgegeben werden 
	   			r�ckgabeWert[1] = 0;
	   			
	   		}
	           
	       }
		
		return;
	}
	}





// Ich habe mich f�r Byte entschieden, da keine Person mehr als 127 Tickets auf einmal an einem Fahrkartenautomaten kauft.