import java.util.Scanner; // Import der Klasse Scanner 
 
public class name_alter
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Guten Tag, wie lautet Ihr Name? ");    
     
    // Die Variable name speichert die erste Eingabe 
    String name = myScanner.next();  
     
    System.out.print("Bitte geben Sie auch Ihr Alter ein. "); 
     
    // Die Variable alter speichert die zweite Eingabe
    byte alter = myScanner.nextByte();  
     
    
     // Gibt Name und Alter aus
    System.out.print("Ihr Name lautet: " + name + " und Sie sind "+ alter +" Jahre alt" );    
 
    myScanner.close(); 
     
  }    
}